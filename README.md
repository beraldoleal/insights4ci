<div align="center">

<img src="https://gitlab.com/beraldoleal/insights4ci/-/raw/main/ui/src/assets/insights4ci-logo.png" alt="Insights4CI" width="250">

![Stability](https://img.shields.io/badge/stability-experimental-red.svg)
![Contributions welcome](https://img.shields.io/badge/contributions-welcome-orange.svg)
![Python](https://img.shields.io/badge/Python-v3.6+-blue.svg)
![VueJS3](https://img.shields.io/badge/VueJS-v3-brightgreen.svg)
![License](https://img.shields.io/badge/license-GPLv2-blue.svg)

Insights4CI is **an experimental project** intended to provide project and
runners maintainers, developers, and QE teams with insights on CI/CD pipelines,
helping them better understand their test pipelines.

[Installation](#installation) •
[How to Contribute](#contributing) •
</div>

![Work in Progress Interface](/assets/print.png "Test result page (WiP)")

## Nice features

<table align="center">
<tr>
  <td align="left" width="30%">
   <strong>Soft Bisect:</strong> Quickly spot the last time a test was passing without the
   need to prepare an environment to run the tests again.  By clicking in any
   test, a details pannel it will show on the right with, in addition to other
   information, a list of the last time your test passed.
  </td>
  <td width="70%">
   <img src="https://gitlab.com/beraldoleal/insights4ci/-/raw/main/assets/soft-bisect.png" align="right">
  </td>
</td></tr>
</table>

## Installing

For convenience, this project has a `docker-compose.yml` file so you can use
`docker-compose` to bootstrap all services (db, api_server, and web client). So
in case you feel comfortable with Docker containers, just run the following
commands and wait a few minutes for the first pulling:


    $ docker-compose build --no-cache
    $ docker-compose up

However, if you don't like to use containers here, you can set up each service
manually.  Please visit each repository for details.

TODO: Create steps to reproduce this in a k8s cluster.

## Populating

TODO: Improve here

## Contributing

This project has just started, so there is no better time for you to
collaborate. 

Please, visit the `CONTRIBUTING.md` file for how to contribute and how to
deploy insights4ci with other methods.

## License

This project is licensed under the terms of the GPL Open Source license (v2)
and is available for free. For details, visit the `LICENSE` file.
